// navbar on scroll - white

function navChange(){
  $('#navbar').css({
      backgroundColor: 'white',
      height: '11vh',
      borderBottom: '1px solid lightgrey'
    });
    $('.hamburger').css({
      color: 'black'
    });
    $('.navbar-brand').css({
      textTransform: 'uppercase',
      color: 'black'
    });
    $('.menu-a').css({
      color: 'black'
    });
}

$(document).on('mousewheel', function(){
  if ($(document).scrollTop() > 0 ) {
    navChange();
  } else {
    $('#navbar').css({
      backgroundColor: 'transparent',
      borderBottom: '0'
    });
    $('.hamburger').css({
      color: 'white'
    });
    $('.navbar-brand').css({
      textTransform: 'capitalize',
      color: 'white'
    });
    $('.menu-a').css({
      color: 'white'
    });
  }
})

//arrow scroll down on click, & navbar change color

$(document).ready(function() {
    $(".arrow-a").click(function(e) {
        e.preventDefault();
        $("html, body").animate({ scrollTop: 570 }, "slow");
        navChange();
    });

});

//arrow scroll - animate

$(function() {
  setTimeout('Arrow()');
});

function Arrow() {
  $('.arrow').animate({
    marginBottom: '-=20px'
  }, 800).animate({
    marginBottom: '+=20px'
  }, 800);
  setTimeout('Arrow()', 1600);
};

//navbar hamburger menu 

$(document).ready( function(){
    $('body').on('click', '.hamburger', function() {
      $('#myNav').animate({
        display: 'block',
        height: '100%'
      }, 'fast');
  });

  $('body').on('click', '.closebtn', function(){
    $('#myNav').animate({
      display: 'none',
      height: '0%'
    }, 'fast');
  });
});


//tablet - scroll


  //picture -- all the way to the top

$(document).ready(function() {
	 $('.content').scrollLeft(200);
});


$('.arrow-top').click(function(){    
    $('.content').animate({
        scrollLeft: 200 //200 ili all the way Left
    }, 1000);
});

$('.arrow-bottom').click(function(){    
    $('.content').animate({
        scrollLeft: 0 // 0 ili all the way Right
    }, 1000);
});





$(document).ready(function() {
    
    //FUNKCII ZA POZADINATA
      function smeniPozadina(){
        $(this).addClass('bg-image-map');
        $(this).css({"color" : "white"});
      }
      function vratiPozadina(){
        $(this).removeClass('bg-image-map');
        $(this).css({"color" : ""});
      }

    //OFF & ON EVENTI ZA POZADINATA NA LAPTOP I MOBILEN
      var alterFunction = function() {
        var ww = document.body.clientWidth;
        if (ww <= 768) {
        $('.thirdHolder').off('mouseover', smeniPozadina);
        $('.thirdHolder').off('mouseout', vratiPozadina);
        } else if (ww >= 769) {
        $('.thirdHolder').on('mouseover', smeniPozadina);
        $('.thirdHolder').on('mouseout', vratiPozadina);
        };
      };
      $(window).resize(function(){
        alterFunction();
      });
      alterFunction();


     //OFF I ON EVENTI ZA TEKSTOT NA LAPTOP I MOBILEN
    $(".none1, .none2, .none3").addClass('none');
    $(".ikona1 , .ikona2, .ikona3").addClass('none')
    var alterClass = function() {
      var mm = document.body.clientWidth;
      if (mm <= 768) {
      $(".ikona1").addClass("block");   
      $(".ikona2").addClass("block");   
      $(".ikona3").addClass("block");   
      $(".more1").click(function () {
            $(".more1").fadeOut("slow",function () {
                 $(".more1").text(($(".more1").text() == 'MORE') ? 'LESS' : 'MORE').fadeIn("slow");
            })
            $(".none1").fadeToggle(1000);
         })
        $(".more2").click(function () {
            $(".more2").fadeOut("slow",function () {
                 $(".more2").text(($(".more2").text() == 'MORE') ? 'LESS' : 'MORE').fadeIn("slow");
            })
            $(".none2").fadeToggle(1000);
         }) 
         $(".more3").click(function () {
            $(".more3").fadeOut("slow",function () {
                 $(".more3").text(($(".more3").text() == 'MORE') ? 'LESS' : 'MORE').fadeIn("slow");
            })
            $(".none3").fadeToggle(1000);
         })   
      } else if (mm >= 769) { 
      $(".thirdHolder1").hover(function(){
        $(".ikona1").show();
        $(".none1").fadeIn(1200)
      }, function() {
        $(".ikona1").hide();
        $(".none1").hide();
      });
      $(".thirdHolder2").hover(function(){
        $(".ikona2").show();
        $(".none2").fadeIn(1200)
      }, function() {
        $(".ikona2").hide();
        $(".none2").hide();
      });
      $(".thirdHolder3").hover(function(){
        $(".ikona3").show();
        $(".none3").fadeIn(1200)
      }, function() {
        $(".ikona3").hide();
        $(".none3").hide();
      });
      $(".more1 , .more2 , .more3").addClass("none")
      };
    };
    $(window).resize(function(){
        alterClass();
    });
     alterClass();




     $(document).on("mousewheel" , function(){
      if ($(document).scrollTop() > 150) {
        $(".footer-txt").fadeIn(1000);
        $(".footer-btn").fadeIn(2000);
      } else {
        $(".footer-txt").hide();
        $(".footer-btn").hide();
      }
     })

    






    });







function clockJavaScript(argument) {
      var date = new Date();

      var seconds = date.getSeconds();
      var secondsDegrees = (seconds/60)*360 + 90;
      var secondsHand = document.querySelector(".seconds-hand")
      secondsHand.style.transform = `rotate(${secondsDegrees}deg)`;
      var secondsHand1 = document.querySelector(".seconds-hand1")
      secondsHand1.style.transform = `rotate(${secondsDegrees}deg)`;
      var secondsHand2 = document.querySelector(".seconds-hand2")
      secondsHand2.style.transform = `rotate(${secondsDegrees}deg)`;
      var secondsHand3 = document.querySelector(".seconds-hand3")
      secondsHand3.style.transform = `rotate(${secondsDegrees}deg)`;
      var secondsHand4 = document.querySelector(".seconds-hand4")
      secondsHand4.style.transform = `rotate(${secondsDegrees}deg)`;
      var secondsHand5 = document.querySelector(".seconds-hand5")
      secondsHand5.style.transform = `rotate(${secondsDegrees}deg)`;


      var minutes = date.getMinutes();
      var minutesDegrees = (minutes/60)*360 + 90;
      var minutesHand = document.querySelector(".minutes-hand")
      minutesHand.style.transform = `rotate(${minutesDegrees}deg)`;


      var minutesHand1 = document.querySelector(".minutes-hand1")
      minutesHand1.style.transform = `rotate(${minutesDegrees}deg)`;
      var minutesHand2 = document.querySelector(".minutes-hand2")
      minutesHand2.style.transform = `rotate(${minutesDegrees}deg)`;
      var minutesHand3 = document.querySelector(".minutes-hand3")
      minutesHand3.style.transform = `rotate(${minutesDegrees}deg)`;
      var minutesHand4 = document.querySelector(".minutes-hand4")
      minutesHand4.style.transform = `rotate(${minutesDegrees}deg)`;
      var minutesHand5 = document.querySelector(".minutes-hand5")
      minutesHand5.style.transform = `rotate(${minutesDegrees}deg)`;


      var hours = date.getHours();      

      var hoursNewYork = hours - 6;
      var hoursDegreesNewYork = (hoursNewYork/12)*360 + 90;
      var hoursHand = document.querySelector(".hours-hand")
      hoursHand.style.transform = `rotate(${hoursDegreesNewYork}deg)`;

      var hoursLondon = hours -1;
      var hoursDegreesLondon = (hoursLondon/12)*360 + 90;
      var hoursHand1 = document.querySelector(".hours-hand1")
      hoursHand1.style.transform = `rotate(${hoursDegreesLondon}deg)`;

      
      var hoursHand2 = document.querySelector(".hours-hand2")
      hoursHand2.style.transform = `rotate(${hoursDegreesNewYork}deg)`;

      var hoursHand3 = document.querySelector(".hours-hand3")
      hoursHand3.style.transform = `rotate(${hoursDegreesNewYork}deg)`;

      var hoursBucharest = hours + 1;
      var hoursDegreesBucharest = (hoursBucharest/12)*360 + 90;
      var hoursHand4 = document.querySelector(".hours-hand4")
      hoursHand4.style.transform = `rotate(${hoursDegreesBucharest}deg)`;

      var hoursWarsaw = hours;
      var hoursDegreesWarsaw = (hoursWarsaw/12)*360 + 90;
      var hoursHand5 = document.querySelector(".hours-hand5")
      hoursHand5.style.transform = `rotate(${hoursDegreesWarsaw}deg)`;
    }
    setInterval(clockJavaScript, 1000);